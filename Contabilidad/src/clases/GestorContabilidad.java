package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;

	public GestorContabilidad() {
		this.listaFacturas = new ArrayList<Factura>();
		this.listaClientes = new ArrayList<Cliente>();
	}
	
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	
//	TESTS EN TestFacturasClientes.java
	public int cantidadFacturasPorCliente(String dni) {
		
		int numeroFacturas = 0;
		
		for(Factura unaFactura : listaFacturas) {
			if(unaFactura.getCliente().getDni().equalsIgnoreCase(dni)) {
				numeroFacturas++;
			}
		}		
		
		return numeroFacturas;
	}
	
//	TESTS EN TestFacturasClientes.java
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
		Factura unaFactura = null;
		
		for(Factura otraFactura : listaFacturas) {
			if(otraFactura.getCodigoFactura().equals(codigoFactura)) {
				unaFactura = otraFactura;
			}
		}
		
		Cliente unCliente = null;
		
		for(Cliente OtroCliente : listaClientes) {
			if(OtroCliente.getDni().equalsIgnoreCase(dni)) {
				unCliente = OtroCliente;
			}
		}
		
		if(unaFactura != null && unCliente != null) {
			unaFactura.setCliente(unCliente);
		}
	}
	
//	TESTS EN TestsFacturas2.java
	public void eliminarFacturas(String codigo){
		Factura factura = new Factura();
		for(Factura unaFactura : listaFacturas){
			if(unaFactura.getCodigoFactura().equals(codigo)){
				factura = unaFactura;
			}
		}
		listaFacturas.remove(factura);
	}
	
//	TESTS EN TestsFacturas2.java
	public float calcularFacturacionAnual(int anno){
		
		float totalFacturacion = 0;
		
		for(Factura unaFactura : listaFacturas){
			if(unaFactura.getFecha().getYear() == anno){
				totalFacturacion += unaFactura.calcularPrecioTotal();
			}
		}
		
		return totalFacturacion;
	}
	
//	TESTS EN TestsFacturas1.java
	public Factura facturaMasCara(){
		Factura unaFactura = null;		
		float precioTotal = 0;
		for (int i = 0; i < listaFacturas.size(); i++) {
			if (i == 0) {
				unaFactura = listaFacturas.get(i);
				precioTotal = unaFactura.calcularPrecioTotal();
			} else {
				if (precioTotal < listaFacturas.get(i).calcularPrecioTotal()) {
					unaFactura = listaFacturas.get(i);
					precioTotal = this.listaFacturas.get(i).calcularPrecioTotal();
				}
			}
		}
		return unaFactura;
	}
	
//	TESTS EN TestsFacturas1.java
	public void crearFactura(Factura unaFactura){
		if(listaFacturas.size() == 0) {
			listaFacturas.add(unaFactura);				
		}
		boolean available = false;
		for(Factura otroFactura : listaFacturas){
			if(otroFactura.getCodigoFactura().equalsIgnoreCase(unaFactura.getCodigoFactura())){
				available = true;
			}
		}
		if(!available) {
			listaFacturas.add(unaFactura);
		}
	}

	
//	TESTS EN TestsFacturas1.java
	public Factura buscarFactura(String codigo) {		
		for (Factura unaFactura : listaFacturas) {
			if(unaFactura.getCodigoFactura().equalsIgnoreCase(codigo)) {
				return unaFactura;
			}
		}
		return null;
	}
	
//	TESTS EN TestsClientes2.java
	public void eliminarCliente(String dni){
		Cliente cliente = new Cliente();
		for(Cliente unCliente : listaClientes){
			if(unCliente.getDni().equalsIgnoreCase(dni)){
				cliente = unCliente;
			}
		}
		listaClientes.remove(cliente);
	}

//	TESTS EN TestsClientes2.java
	public Cliente clienteMasAntiguo(){
		Cliente cliente = null;
		for (int i = 0; i < listaClientes.size(); i++) {
			if (i == 0) {
				cliente = listaClientes.get(i);
			} else {
				if (listaClientes.get(i).getFecha().compareTo(cliente.getFecha()) < 0) {
					cliente = listaClientes.get(i);
				}
			}

		}
		return cliente;
	}
	
//	TESTS EN TestsClientes1.java
	public void altaCliente(Cliente client){
		if(listaClientes.size() == 0) {
			listaClientes.add(client);				
		}
		boolean available = false;
		for(Cliente otroCliente : listaClientes){
			if(otroCliente.getDni().equalsIgnoreCase(client.getDni())){
				available = true;
			}
		}
		if(!available) {
			listaClientes.add(client);
		}
	}

//	TESTS EN TestsClientes1.java
	public Cliente buscarCliente(String dni) {		
		for (Cliente client : listaClientes) {
			if(client.getDni().equals(dni)) {
				return client;
			}
		}
		return null;
	}

}
