package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.*;

public class TestsClientes2 {
	static GestorContabilidad gestorContabilidad;

	@BeforeClass
	public static void prepararClasePruebas(){
		gestorContabilidad = new GestorContabilidad();
	}
	
	@After
	public void reset(){
		gestorContabilidad.getListaClientes().clear();
	}	

	
//	METODO ELIMINAR CLIENTE
	/*
	 * COMPRUEBA QUE EL METODO ELIMINA CORRECTAMENTE AL CLIENTE
	 */
	@Test
	public void eliminarCliente(){
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(expected);
		gestorContabilidad.eliminarCliente(expected.getDni());
		boolean actual = gestorContabilidad.getListaClientes().contains(expected);
		assertFalse(actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO ELIMINA CORRECTAMENTE AL CLIENTE CUANDO HAY VARIOS CLIENTES
	 */
	@Test
	public void eliminarClienteVariosClientes(){
		Cliente otroCliente = new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"));
		gestorContabilidad.getListaClientes().add(otroCliente);
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(expected);
		gestorContabilidad.eliminarCliente(expected.getDni());
		boolean actual = gestorContabilidad.getListaClientes().contains(expected);
		assertFalse(actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CUANDO VA A ELIMINAR A UN CLIENTE QUE NO EXISTE
	 */
	@Test
	public void eliminarClienteNoExisteSinClientes(){
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.eliminarCliente("25698734T");
		boolean actual = gestorContabilidad.getListaClientes().contains(expected);
		assertFalse(actual);	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CUANDO VA A ELIMINAR A UN CLIENTE QUE NO EXISTE
	 * CUANDO HAY CLIENTES
	 */
	@Test
	public void eliminarClienteNoExisteConClientes(){
		Cliente otroCliente = new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"));
		gestorContabilidad.getListaClientes().add(otroCliente);
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.eliminarCliente("25698734T");
		boolean actual = gestorContabilidad.getListaClientes().contains(expected);
		assertFalse(actual);	}
	
//	METODO CLIENTE MAS ANTIGUO
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE NULL CUANDO NO HAY CLIENTES
	 */
	@Test
	public void clienteMasAntiguoSinClientes(){
		assertNull(gestorContabilidad.clienteMasAntiguo());
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE
	 */
	@Test
	public void clienteMasAntiguoUnCliente(){
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(expected);
		
		Cliente actual = gestorContabilidad.clienteMasAntiguo();
		
		assertSame(expected, actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE CUANDO HAY VARIOS CLIENTES
	 */
	@Test
	public void clienteMasAntiguoVariosClientes(){
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(unCliente);
		
		Cliente otroCliente = new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"));
		gestorContabilidad.getListaClientes().add(otroCliente);
		
		Cliente expected = new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30"));
		gestorContabilidad.getListaClientes().add(expected);
			
		Cliente actual = gestorContabilidad.clienteMasAntiguo();
				
		assertSame(expected, actual);
	}
}
