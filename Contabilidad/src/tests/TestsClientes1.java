package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.*;

public class TestsClientes1 {
	static GestorContabilidad gestorContabilidad;

	@BeforeClass
	public static void prepararClasePruebas(){
		gestorContabilidad = new GestorContabilidad();
	}
	
	@After
	public void reset(){
		gestorContabilidad.getListaClientes().clear();
	}	

	
//	METODO ALTA CLIENTE
	/*
	 * COMPRUEBA QUE EL METODO DA DE ALTA CORRECTAMENTE A UN CLIENTE CUANDO NO HAY CLIENTES
	 */
	@Test
	public void altaClienteSinClientes(){
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.altaCliente(unCliente);
		
		assertTrue(gestorContabilidad.getListaClientes().contains(unCliente));
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DA DE ALTA CORRECTAMENTE A UN CLIENTE CUANDO SI HAY CLIENTES
	 */
	@Test
	public void altaClienteConClientesDiferentes(){
		gestorContabilidad.getListaClientes().add(new Cliente("Juan", 
				"29321587J", LocalDate.parse("2014-07-24")));
		
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.altaCliente(unCliente);
		
		assertTrue(gestorContabilidad.getListaClientes().contains(unCliente));
	}
	
	/*
	 * COMPRUEBA QUE EL METODO NO DA DE ALTA CUANDO HAY UN CLIENTE CON EL MISMO DNI
	 */
	@Test
	public void altaClienteConClientesRepetidos(){
		gestorContabilidad.getListaClientes().add(new Cliente("Juan", 
				"29321587J", LocalDate.parse("2014-07-24")));
		gestorContabilidad.getListaClientes().add(new Cliente("Mar�a", 
				"25698734T", LocalDate.parse("2013-11-30")));	
		
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.altaCliente(unCliente);
		
		assertFalse(gestorContabilidad.getListaClientes().contains(unCliente));
	}

//	METODO BUSCAR CLIENTE
	/*
	 * COMPRUEBA QUE EL METODO BUSCA CORRECTAMENTE AL CLIENTE
	 */
	@Test
	public void buscarCliente(){
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(expected);
		
		Cliente actual = gestorContabilidad.buscarCliente("25698734T");
		
		assertSame(expected, actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE DEVUELVE NULL CUANDO NO HAY CLIENTES
	 */
	@Test
	public void clienteNoEncontradoSinClientes(){
		Cliente actual = gestorContabilidad.buscarCliente("25698734T");
		
		assertNull(actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE NULL CUANDO NO HA ENCONTRADO EL CLIENTE ESPECIFICADO
	 */
	@Test
	public void clienteNoEncontradoConClientes(){
		Cliente clientePrueba = new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(clientePrueba);
		
		Cliente actual = gestorContabilidad.buscarCliente("25698734T");
		
		assertNull(actual);
	}

	
}
