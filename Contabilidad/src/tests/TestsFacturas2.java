package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.*;

public class TestsFacturas2 {
	
	static GestorContabilidad gestorContabilidad;

	@BeforeClass
	public static void prepararClasePruebas(){
		gestorContabilidad = new GestorContabilidad();
	}
	
	@After
	public void reset(){
		gestorContabilidad.getListaFacturas().clear();
	}
	
//	METODO ELIMINAR FACTURAS
	/*
	 * COMPRUEBA QUE EL METODO ELIMINA LA FACTURA
	 */
	@Test
	public void eliminarFactura(){
		Factura expected = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6,
				new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05")));
		gestorContabilidad.getListaFacturas().add(expected);
		gestorContabilidad.eliminarFacturas(expected.getCodigoFactura());
		assertFalse(gestorContabilidad.getListaFacturas().contains(expected));
	}
	
	/*
	 * COMPRUEBA QUE EL METODO ELIMINA CORRECTAMENTE LA FACTURA CUANDO HAY VARIAS FACTURAS
	 */
	@Test
	public void eliminarFacturaVariasFacturas(){
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24")));
		gestorContabilidad.getListaFacturas().add(otraFactura);
		
		Factura expected = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30")));
		gestorContabilidad.getListaFacturas().add(expected);
		gestorContabilidad.eliminarFacturas(expected.getCodigoFactura());
		assertFalse(gestorContabilidad.getListaFacturas().contains(expected));
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CUANDO ELIMINAMOS UNA FACTURA QUE NO EXISTE
	 */
	@Test
	public void eliminarFacturaNoExisteSinFacturas(){
		Factura expected = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30")));
		gestorContabilidad.eliminarFacturas(expected.getCodigoFactura());
		assertFalse(gestorContabilidad.getListaFacturas().contains(expected));
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CUANDO ELIMINA UNA FACTURA PERO EXISTEN FACTURAS
	 */
	@Test
	public void eliminarFacturaNoExisteConFacturas(){
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24")));
		gestorContabilidad.getListaFacturas().add(otraFactura);
		
		Factura expected = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30")));
		gestorContabilidad.eliminarFacturas(expected.getCodigoFactura());
		assertFalse(gestorContabilidad.getListaFacturas().contains(expected));
	}
	
//	METODO FACTURACION ANUAL
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE
	 */
	@Test 
	public void facturacionAnual(){		
		Factura otraFacturaDistinta = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6,
				new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05")));
		gestorContabilidad.getListaFacturas().add(otraFacturaDistinta);
		
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24")));
		gestorContabilidad.getListaFacturas().add(otraFactura);
		
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30")));
		gestorContabilidad.getListaFacturas().add(unaFactura);
		
		float expected = otraFacturaDistinta.calcularPrecioTotal() + otraFactura.calcularPrecioTotal();
		float actual = gestorContabilidad.calcularFacturacionAnual(2017);
		
		assertEquals(expected, actual, 0);		
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE 0 CUANDO NO HAY CLIENTES
	 */
	@Test
	public void facturacionAnualSinClientes(){
		float expected = 0;
		float actual = gestorContabilidad.calcularFacturacionAnual(2017);		
		assertEquals(expected, actual, 0);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE CUANDO NO ENCUENTRA NINGUNA FACTURA
	 * CON ESE ANNO
	 */
	@Test
	public void facturacionAnualConClientesSinAnno(){
		
		Factura otraFacturaDistinta = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6,
				new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05")));
		gestorContabilidad.getListaFacturas().add(otraFacturaDistinta);
		
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24")));
		gestorContabilidad.getListaFacturas().add(otraFactura);
		
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30")));
		gestorContabilidad.getListaFacturas().add(unaFactura);
		
		float expected = 0;
		
		float actual = gestorContabilidad.calcularFacturacionAnual(2012);		
		assertEquals(expected, actual, 0);
	}

}
