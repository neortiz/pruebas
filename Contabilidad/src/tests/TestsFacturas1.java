package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.*;

public class TestsFacturas1 {
	
	static GestorContabilidad gestorContabilidad;

	@BeforeClass
	public static void prepararClasePruebas(){
		gestorContabilidad = new GestorContabilidad();
	}
	
	@After
	public void reset(){
		gestorContabilidad.getListaFacturas().clear();
	}
	
	
//	METODO FACTURA MAS CARA
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE NULO CUANDO NO HAY FACTURAS
	 */
	@Test
	public void facturaMasCaraSinFacturas(){
		assertNull(gestorContabilidad.facturaMasCara());
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE
	 */
	@Test
	public void facturaMasCaraUnaFactura(){
		Cliente unCliente = new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05"));
		Factura expected = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6, unCliente);
		gestorContabilidad.getListaFacturas().add(expected);
		
		Factura actual = gestorContabilidad.facturaMasCara();
		
		assertSame(expected, actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE CUANDO HAY VARIAS FACTURAS
	 */
	@Test
	public void facturaMasCaraVariasFacturas(){
		Cliente unCliente = new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05"));
		Factura expected = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6, unCliente);
		gestorContabilidad.getListaFacturas().add(expected);
		
		Factura otraFactura = new Factura("2", LocalDate.parse("2016-10-25"), "Bebidas", 3.25f, 5,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24")));
		gestorContabilidad.getListaFacturas().add(otraFactura);
		
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30")));
		gestorContabilidad.getListaFacturas().add(unaFactura);
			
		Factura actual = gestorContabilidad.facturaMasCara();
				
		assertSame(expected, actual);
	}
	
//	METODO CREAR FACTURA
	/*
	 * COMPRUEBA QUE EL METODO CREA UNA FACTURA CORRECTAMENTE CUANDO NO HAY FACTURAS PREVIAS
	 */
	@Test
	public void crearFacturaSinFacturas(){
		Cliente unCliente = new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05"));
		Factura expected = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6, unCliente);
		gestorContabilidad.crearFactura(expected);
		
		assertTrue(gestorContabilidad.getListaFacturas().contains(expected));
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE CUANDO HAY FACTURAS PREVIAS NO REPETIDAS
	 */
	@Test
	public void crearFacturaConFacturasDiferentes(){
		gestorContabilidad.getListaFacturas().add(new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"))));
		
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		Factura expected = new Factura("2", LocalDate.parse("2016-10-25"), "Bebidas", 3.25f, 5, unCliente);
		gestorContabilidad.crearFactura(expected);
		
		assertTrue(gestorContabilidad.getListaFacturas().contains(expected));
	}
	
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE CUANDO HAY FACTURAS REPETIDAS
	 */
	@Test
	public void crearFacturaConFacturasRepetidas(){
		gestorContabilidad.getListaFacturas().add(new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"))));
		gestorContabilidad.getListaFacturas().add(new Factura("2", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30"))));			
		
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		Factura expected = new Factura("2", LocalDate.parse("2016-10-25"), "Bebidas", 3.25f, 5, unCliente);
		gestorContabilidad.crearFactura(expected);
		
		assertFalse(gestorContabilidad.getListaFacturas().contains(expected));
	}
	
//	METODO BUSCAR FACTURA
	/*
	 * COMPRUEBA QUE EL METODO FUNCIONA CORRECTAMENTE
	 */
	@Test
	public void buscarFactura(){
		Cliente unCliente = new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05"));
		Factura expected = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6, unCliente);
		gestorContabilidad.getListaFacturas().add(expected);
		
		Factura actual = gestorContabilidad.buscarFactura("1");
		
		assertSame(expected, actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE NULL CUANDO LA FACTURA NO EXISTE
	 */
	@Test
	public void facturaNoEncontradaSinFacturas(){
		Factura actual = gestorContabilidad.buscarFactura("1");
		
		assertNull(actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE NULL CUANDO LA FACTURA NO EXISTE CUANDO HAY MAS FACTURAS
	 */
	@Test
	public void facturaNoEncontradaConFacturas(){
		Cliente unCliente = new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05"));
		Factura expected = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6, unCliente);
		gestorContabilidad.getListaFacturas().add(expected);
		
		Factura actual = gestorContabilidad.buscarFactura("2");
		
		assertNull(actual);
	}
	
}
