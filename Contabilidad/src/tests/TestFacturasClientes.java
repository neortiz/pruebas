package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.*;

public class TestFacturasClientes {
	
	static GestorContabilidad gestorContabilidad;

	@BeforeClass
	public static void prepararClasePruebas(){
		gestorContabilidad = new GestorContabilidad();
	}
	
	@After
	public void reset(){
		gestorContabilidad.getListaFacturas().clear();
		gestorContabilidad.getListaClientes().clear();
	}
	
//	METODO ASIGNAR CLIENTE A FACTURA
	/*
	 * COMPRUEBA QUE EL METODO ASIGNA UN CLIENTE A UNA FACTURA
	 */
	@Test
	public void asignarClienteAFactura() {
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(expected);
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5, null);
		gestorContabilidad.getListaFacturas().add(unaFactura);
		gestorContabilidad.asignarClienteAFactura(expected.getDni(), unaFactura.getCodigoFactura());
		Cliente actual = gestorContabilidad.getListaFacturas().get(gestorContabilidad.getListaFacturas().
				indexOf(unaFactura)).getCliente();
		assertSame(expected, actual);
	}
	/*
	 * COMPRUEBA QUE EL METODO NO INTRODUCE UN CLIENTE QUE NO EXISTE
	 */
	@Test
	public void asignarClienteNoExisteAFactura() {
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5, null);
		gestorContabilidad.getListaFacturas().add(unaFactura);
		gestorContabilidad.asignarClienteAFactura(unCliente.getDni(), unaFactura.getCodigoFactura());
		Cliente actual = gestorContabilidad.getListaFacturas().get(gestorContabilidad.getListaFacturas().
				indexOf(unaFactura)).getCliente();
		assertNull(actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO NO INTRODUCE UN CLIENTE EN UNA FACTURA QUE NO EXISTE
	 */
	@Test
	public void asignarClienteAFacturaNoExiste() {
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(unCliente);
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5, null);
		gestorContabilidad.asignarClienteAFactura(unCliente.getDni(), unaFactura.getCodigoFactura());
		Cliente actual = unaFactura.getCliente();
		assertNull(actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO ASIGNA UN CLIENTE A UNA FACTURA CUANDO HAY VARIOS CLIENTES 
	 * Y VARIAS FACTURAS
	 */
	@Test
	public void asignarClienteAFacturaVariosClientesVariasFacturas() {
		Cliente unCliente = new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"));
		gestorContabilidad.getListaClientes().add(unCliente);
		Cliente expected = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		gestorContabilidad.getListaClientes().add(expected);
		
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5,
				new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24")));
		gestorContabilidad.getListaFacturas().add(otraFactura);		
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5,
				new Cliente("Mar�a", "25698734T", LocalDate.parse("2013-11-30")));
		gestorContabilidad.getListaFacturas().add(unaFactura);
		
		gestorContabilidad.asignarClienteAFactura(expected.getDni(), unaFactura.getCodigoFactura());
		
		Cliente actual = gestorContabilidad.getListaFacturas().get(gestorContabilidad.getListaFacturas().
				indexOf(unaFactura)).getCliente();
		
		assertSame(expected, actual);

	}
	
//	METODO CANTIDAD DE FACTURAS POR CLIENTE
	/*
	 * COMPRUEBA QUE EL METODO CUENTA CORRECTAMENTE LAS FACTURAS QUE TIENE UN CLIENTE
	 */
	@Test
	public void numeroFacturaPorCliente() {
		Cliente unCliente = new Cliente("Ana", "25698734T", LocalDate.parse("2017-12-05"));
		
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5, unCliente);
		gestorContabilidad.getListaFacturas().add(otraFactura);
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5, unCliente);
		gestorContabilidad.getListaFacturas().add(unaFactura);
		
		int actual = 2;
		int expected = gestorContabilidad.cantidadFacturasPorCliente("25698734T");
		
		assertEquals(expected, actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE 0 CUANDO EL CLIENTE NO TIENE FACTURAS
	 */
	@Test
	public void numeroFacturaPorClienteNoExiste() {
		Cliente otroCliente = new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"));
		
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5, otroCliente);
		gestorContabilidad.getListaFacturas().add(otraFactura);
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5, otroCliente);
		gestorContabilidad.getListaFacturas().add(unaFactura);
		
		int actual = 0;
		int expected = gestorContabilidad.cantidadFacturasPorCliente("25698734T");
		
		assertEquals(expected, actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE 0 CUANDO NO HAY FACTURAS
	 */
	@Test
	public void numeroFacturaPorClienteFacturaNoExiste() {		
		int actual = 0;
		int expected = gestorContabilidad.cantidadFacturasPorCliente("29321587J");
		
		assertEquals(expected, actual);
	}
	
	/*
	 * COMPRUEBA QUE EL METODO DEVUELVE 0 CUANDO NO TODAS LAS FACTURAS SON DEL CLIENTE
	 */
	@Test
	public void numeroFacturaPorClienteVariasFacturas() {
		Cliente otroCliente = new Cliente("Juan", "29321587J", LocalDate.parse("2014-07-24"));
		
		Factura otraFactura = new Factura("2", LocalDate.parse("2017-10-25"), "Bebidas", 3.25f, 5, otroCliente);
		gestorContabilidad.getListaFacturas().add(otraFactura);
		Factura unaFactura = new Factura("3", LocalDate.parse("2015-06-17"), "Detergente", 9.75f, 5, otroCliente);
		gestorContabilidad.getListaFacturas().add(unaFactura);
		Factura otraFacturaDistinta = new Factura("1", LocalDate.parse("2017-12-05"), "Comida", 19.25f, 6,
				new Cliente("Ana", "25698734H", LocalDate.parse("2017-12-05")));
		gestorContabilidad.getListaFacturas().add(otraFacturaDistinta);
		
		int actual = 2;
		int expected = gestorContabilidad.cantidadFacturasPorCliente("29321587J");
		
		assertEquals(expected, actual);
	}
}